/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that models playing card Objects. Cards have 
 * a value 
 * a type
 * A color
 * There are 112 cards in a deck.
 * This code is to be used in ICE1. When you create your own branch,
 * add your name as a modifier.
 * @author dancye
 * @modifier Yash Lochab and Nishu Rani
 */
public class Card {

   private String type; //number cards, special cards- reverse, skip, draw 2 etc
   private int value;//0-50
   private String color;

   public static final String [] TYPES = {"Number", "Reverse", "Skip", "Wild", "Draw2", "WildDraw4"};
   public static final String [] COLORS={"Red", "Green", "Blue", "Yellow"};
   /**
     * @return the suit
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(int value) {
        this.value = value;
    }
    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    
}
