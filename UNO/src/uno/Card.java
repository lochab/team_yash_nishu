package uno;
//Represents a single card
public class Card {

    public static final boolean PRINT = false;

    private Uno_Player.Color color;
    private Uno_Player.Type type;
    private int value;

   //constructor
    public Card(Uno_Player.Color color, Uno_Player.Type type) {
        this.color = color;
        this.type = type;
        this.value = -1;
    }

  //constructor
    public Card(Uno_Player.Color color, int number) {
        this.color = color;
        this.type = Uno_Player.Type.VALUE;
        this.value = number;
    }

  //constructor
    public Card(Uno_Player.Color color, Uno_Player.Type type, int value) {
        this.color = color;
        this.type = type;
        this.value = value;
    }
// getter method for color
    public Uno_Player.Color getColor() {
        return color;
    }

   //getter method for type 
    public Uno_Player.Type getType() {
        return type;
    }

    //getter method for value
    public int getValue() {
        return value;
    }
    
    //method to calculate forfeit cost
    public int forfeit() {
        if (type == Uno_Player.Type.SKIP || type == Uno_Player.Type.REVERSE ||
            type == Uno_Player.Type.DRAW_TWO) {
            return 20;
        }
        if (type == Uno_Player.Type.WILD || type == Uno_Player.Type.WILD_D4) {
            return 50;
        }
        if (type == Uno_Player.Type.VALUE) {
            return value;
        }
        System.out.println("Illegal card!!");
        return -10000;
    }

    // method to continue play
    public boolean PlayOn(Card c, Uno_Player.Color col) {
        if (type == Uno_Player.Type.WILD ||
            type == Uno_Player.Type.WILD_D4 ||
            color == c.color ||
            color == col ||
            (type == c.type && type != Uno_Player.Type.VALUE) ||
            value == c.value && type == Uno_Player.Type.VALUE && c.type == Uno_Player.Type.VALUE)
        {
            return true;
        }
        return false;
    }

    
    public boolean followedByCall() {
        return type == Uno_Player.Type.WILD || type == Uno_Player.Type.WILD_D4;
    }

    //method that define sthe effect of a card
    void Card_Effect(Game game) throws Empty_Exception {
        switch (type) {
            case SKIP:
                game.nextPlayerMove();
                game.nextPlayerMove();
                break;
            case REVERSE:
                game.reverseMove();
                game.nextPlayerMove();
                break;
            case DRAW_TWO:
                nextDraw(game);
                nextDraw(game);
                game.nextPlayerMove();
                game.nextPlayerMove();
                break;
            case WILD_D4:
                nextDraw(game);
                nextDraw(game);
                nextDraw(game);
                nextDraw(game);
                game.nextPlayerMove();
                game.nextPlayerMove();
                break;
            default:
                game.nextPlayerMove();
                break;
        }
    }
// method that defines next draw to be made
    private void nextDraw(Game game) throws Empty_Exception {
        int nextPlayer = game.getNextPlayer();
        Card drawn;
        try {
            drawn = game.deck.draw();
        }
        catch (Empty_Exception e) {
            game.print("...deck exhausted, remixing...");
            game.deck.Shuffle_Mix();
            drawn = game.deck.draw();
        }
        game.h[nextPlayer].addCard(drawn);
        //game.println("  Player #" + nextPlayer + " draws " + drawnCard + ".");
        game.println("  " + game.h[nextPlayer].getPlayerName() + " draws " +
            drawn + ".");
    }

  
    //check if it is a same card
    public boolean isTheSameCard(Card card) {
    	return (color == card.getColor() &&
    			type == card.getType() &&
    			value == card.getValue());
    }
    
    public String toString() {
        String val = "";
        if (PRINT) {
            switch (color) {
                case RED:
                    val += "\033[31m";
                    break;
                case YELLOW:
                    val += "\033[33m";
                    break;
                case GREEN:
                    val += "\033[32m";
                    break;
                case BLUE:
                    val += "\033[34m";
                    break;
                case NONE:
                    val += "\033[1m";
                    break;
            }
        }
        else {
            switch (color) {
                case RED:
                    val += "R";
                    break;
                case YELLOW:
                    val += "Y";
                    break;
                case GREEN:
                    val += "G";
                    break;
                case BLUE:
                    val += "B";
                    break;
                case NONE:
                    val += "";
                    break;
            }
        }
        switch (type) {
            case VALUE:
                val += value;
                break;
            case SKIP:
                val += "S";
                break;
            case REVERSE:
                val += "R";
                break;
            case WILD:
                val += "W";
                break;
            case DRAW_TWO:
                val += "+2";
                break;
            case WILD_D4:
                val += "W4";
                break;
        }
        if (PRINT) {
            val += "\033[37m\033[0m";
        }
        return val;
    }

}
