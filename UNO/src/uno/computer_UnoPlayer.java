
package uno;

import java.util.List;
//represents computer player
public class computer_UnoPlayer implements Uno_Player {

    public int play(List<Card> cardsInHand, Card cardOnTop, Color color,
        Game_State state) {

        int i = 0;

        
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardOnTop.getColor ( ) == Color.NONE )
            {
                if ( cardsInHand.get ( i ).getColor ( ) == color && cardsInHand.get ( i ).getValue ( ) > 5 )
                {
                    return i;
                }
                if ( cardsInHand.get ( i ).getColor ( ) == color && cardsInHand.get ( i ).getValue ( ) <= 5 )
                {
                    return i;
                }
            }
        }
       
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getType ( ) == cardOnTop.getType ( ) && cardOnTop.getType ( ) != Type.VALUE &&
                 cardOnTop.getColor ( ) != Color.NONE )
            {
                return i;
            }
        }
        
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == cardOnTop.getColor ( ) && cardsInHand.get ( i ).getValue ( ) >
                 cardOnTop.getValue ( ) )
            {
                return i;
            }
        }
       
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getValue ( ) == cardOnTop.getValue ( ) && cardsInHand.get ( i ).getValue ( ) != -1 )
            {
                return i;
            }
        }
       
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == cardOnTop.getColor ( ) && cardsInHand.get ( i ).getValue ( ) > 5 )
            {
                return i;
            }
            if ( cardsInHand.get ( i ).getColor ( ) == cardOnTop.getColor ( ) && cardsInHand.get ( i ).getValue ( ) <= 5 )
            {
                return i;
            }
        }
       
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == Color.NONE )
            {
                return i;
            }
        }

        return -1;
    }


   
    public Color addColor(List<Card> cardsInHand) {

        int i = 0;
        int numColor [ ] = new int [ 4 ];
        int highestColor = 0;
        Color calledColor = Color.RED;

         for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == Color.RED )
            {
                numColor [ 0 ]++;
            }
        }
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == Color.BLUE )
            {
                numColor [ 1 ]++;
            }
        }
         for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == Color.GREEN )
            {
                numColor [ 2 ]++;
            }
        }
        for ( i = 0; i < cardsInHand.size ( ); i++ )
        {
            if ( cardsInHand.get ( i ).getColor ( ) == Color.YELLOW )
            {
                numColor [ 3 ]++;
            }
        }
         for ( i = 0; i < numColor.length; i++ )
        {
            if ( numColor [ i ] > highestColor )
            {
                highestColor = numColor [ i ];
            }
        }
        if ( highestColor == numColor [ 0 ] )
        {
            calledColor = Color.RED;
        }
        else if ( highestColor == numColor [ 1 ] )
        {
            calledColor = Color.BLUE;
        }
        else if ( highestColor == numColor [ 2 ] )
        {
            calledColor = Color.GREEN;
        }
        else if ( highestColor == numColor [ 3 ] )
        {
            calledColor = Color.YELLOW;
        }

        return calledColor;
    }
 
}