
package uno;

import java.util.ArrayList;


public class Game {

    static final int INIT_HAND_SIZE = 7;

    public enum Direction { FORWARD, BACKWARD };

    private Game_State state;

   
    Deck_of_Cards deck;
    Hand h[];
    Card CardOnTop;
    Direction direction;
    int currentPlayer;
    Uno_Player.Color color;
    Game_Scoreboard scoreBoard;
    Uno_Player.Color mostRecentColorCalled[];

    //constructor
    public Game(Game_Scoreboard scoreboard, ArrayList<String> playerClassList) {
        this.scoreBoard = scoreboard;
        deck = new Deck_of_Cards();
        h = new Hand[scoreboard.getNumPlayers()];
        mostRecentColorCalled =
            new Uno_Player.Color[scoreboard.getNumPlayers()];
        try {
            for (int i=0; i<scoreboard.getNumPlayers(); i++) {
                h[i] = new Hand(playerClassList.get(i),
                    scoreboard.getPlayerList()[i]);
                for (int j=0; j<INIT_HAND_SIZE; j++) {
                    h[i].addCard(deck.draw());
                }
            }
            CardOnTop = deck.draw();
            while (CardOnTop.followedByCall()) {
                deck.discard(CardOnTop);
                CardOnTop = deck.draw();
            }
        }
        catch (Exception e) {
            System.out.println("Can't deal initial hands!");
            System.exit(1);
        }
        direction = Direction.FORWARD;
        currentPlayer =
            new java.util.Random().nextInt(scoreboard.getNumPlayers());
        color = Uno_Player.Color.NONE;
    }
//print current game state
    private void printState() {
        for (int i=0; i<scoreBoard.getNumPlayers(); i++) {
            System.out.println("Hand #" + i + ": " + h[i]);
        }
    }

  // method to get the next player
    public int getNextPlayer() {
        if (direction == Direction.FORWARD) {
            return (currentPlayer + 1) % scoreBoard.getNumPlayers();
        }
        else {
            if (currentPlayer == 0) {
                return scoreBoard.getNumPlayers() - 1;
            }
            else {
                return currentPlayer - 1;
            }
        }
    }
//get next move
    void nextPlayerMove() {
        currentPlayer = getNextPlayer();
    }

    /**
     * Change the direction of the game from clockwise to counterclockwise
    
     */
    void reverseMove() {
        if (direction == Direction.FORWARD) {
            direction = Direction.BACKWARD;
        }
        else {
            direction = Direction.FORWARD;
        }
    }

   //GAme play
    public void Play() {
        println("Initial upcard is " + CardOnTop + ".");
        try {
            while (true) {
                //print("Hand #" + currPlayer + " (" + h[currPlayer] + ")");
                print(h[currentPlayer].getPlayerName() +
                    " (" + h[currentPlayer] + ")");
                Card playedCard = h[currentPlayer].play(this);
                if (playedCard == null) {
                    Card drawnCard;
                    try {
                        drawnCard = deck.draw();
                    }
                    catch (Exception e) {
                        print("...deck exhausted, remixing...");
                        deck.Shuffle_Mix();
                        drawnCard = deck.draw();
                    }
                    h[currentPlayer].addCard(drawnCard);
                    if (h[currentPlayer].isHuman())
                    	print(h[currentPlayer].getPlayerName());
                    print(" has to draw (" + drawnCard + ").");
                    playedCard = h[currentPlayer].play(this);
                }
                if (playedCard != null) {
                    if (h[currentPlayer].isHuman())
                    	print(h[currentPlayer].getPlayerName());
                    print(" plays " + playedCard + " on " + CardOnTop + ".");
                    deck.discard(CardOnTop);
                    CardOnTop = playedCard;
                    if (CardOnTop.followedByCall()) {
                        color = h[currentPlayer].addColor(this);
                        mostRecentColorCalled[currentPlayer] = color;
                        print(" (and calls " + color +
                            ").");
                    }
                    else {
                        color = Uno_Player.Color.NONE;
                    }
                }
                if (h[currentPlayer].isEmpty()) {
                    int roundPoints = 0;
                    for (int j=0; j<scoreBoard.getNumPlayers(); j++) {
                        roundPoints += h[j].countCards();
                    }
                    println("\n" + h[currentPlayer].getPlayerName() +
                        " wins! (and collects " + roundPoints + " points.)");
                    scoreBoard.addToScore(currentPlayer,roundPoints);
                    println("---------------\n" + scoreBoard);
                    return;
                }
                if (h[currentPlayer].size() == 1) {
                    print(" UNO!");
                }
                println("");
                if (playedCard != null) {
                    playedCard.Card_Effect(this);
                }
                else {
                    nextPlayerMove();
                }
            }
        }
        catch (Empty_Exception e) {
            System.out.println("Deck exhausted! This game is a draw.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    void print(String s) {
        if (Uno_Simulation.PRINT_VERBOSE) {
            System.out.print(s);
        }
    }

    void println(String s) {
        if (Uno_Simulation.PRINT_VERBOSE) {
            System.out.println(s);
        }
    }

    /**
     * Return the GameState object, through which the state of the game can
     * be accessed and safely manipulated.
     */
    public Game_State getGameState() {

        return new Game_State(this);
    }

    /**
     * Return the Card that is currently the "up card" in the game.
     */
    public Card getTopCard() {
        return CardOnTop;
    }
}
