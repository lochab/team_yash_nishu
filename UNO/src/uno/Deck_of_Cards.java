
package uno;
//represents a deck of cards
import java.util.ArrayList;
import java.util.Random;


public class Deck_of_Cards {

    
    public static final int DUPLICATE_REGULAR_CARDS = 2;

    public static final int DUPLICATE_ZERO_CARDS = 1;

    public static final int DUPLICATE_SPECIAL_CARDS = 2;

    public static final int WILD_CARDS = 4;

    public static final int WILD_D4_CARDS = 4;

    public static final int SHUFFLE = 1;

    private ArrayList<Card> cardsList = new ArrayList<Card>();
    private ArrayList<Card> discardedCards = new ArrayList<Card>();
    private Random rand;

   // Default constructor
    public Deck_of_Cards() {
        rand = new Random();
        fillDeck();
        shuffle();
    }
    // method to fill the deck
    private void fillDeck() {
        for (int i=1; i<=9; i++) {
            for (int j=0; j<DUPLICATE_REGULAR_CARDS; j++) {
                cardsList.add(new Card(Uno_Player.Color.RED,i));
                cardsList.add(new Card(Uno_Player.Color.YELLOW,i));
                cardsList.add(new Card(Uno_Player.Color.BLUE,i));
                cardsList.add(new Card(Uno_Player.Color.GREEN,i));
            }
        }
        
        for (int j=0; j<DUPLICATE_ZERO_CARDS; j++) {
            cardsList.add(new Card(Uno_Player.Color.RED,0));
            cardsList.add(new Card(Uno_Player.Color.YELLOW,0));
            cardsList.add(new Card(Uno_Player.Color.BLUE,0));
            cardsList.add(new Card(Uno_Player.Color.GREEN,0));
        }
        for (int j=0; j<DUPLICATE_SPECIAL_CARDS; j++) {
            cardsList.add(new Card(Uno_Player.Color.RED,Uno_Player.Type.SKIP));
            cardsList.add(new Card(Uno_Player.Color.YELLOW,Uno_Player.Type.SKIP));
            cardsList.add(new Card(Uno_Player.Color.GREEN,Uno_Player.Type.SKIP));
            cardsList.add(new Card(Uno_Player.Color.BLUE,Uno_Player.Type.SKIP));
            cardsList.add(new Card(Uno_Player.Color.RED,Uno_Player.Type.REVERSE));
            cardsList.add(new Card(Uno_Player.Color.YELLOW,Uno_Player.Type.REVERSE));
            cardsList.add(new Card(Uno_Player.Color.GREEN,Uno_Player.Type.REVERSE));
            cardsList.add(new Card(Uno_Player.Color.BLUE,Uno_Player.Type.REVERSE));
            cardsList.add(new Card(Uno_Player.Color.RED,Uno_Player.Type.DRAW_TWO));
            cardsList.add(new Card(Uno_Player.Color.YELLOW,Uno_Player.Type.DRAW_TWO));
            cardsList.add(new Card(Uno_Player.Color.GREEN,Uno_Player.Type.DRAW_TWO));
            cardsList.add(new Card(Uno_Player.Color.BLUE,Uno_Player.Type.DRAW_TWO));
        }
        for (int i=0; i<WILD_CARDS; i++) {
            cardsList.add(new Card(Uno_Player.Color.NONE,Uno_Player.Type.WILD));
        }
        for (int i=0; i<WILD_D4_CARDS; i++) {
            cardsList.add(new Card(Uno_Player.Color.NONE,Uno_Player.Type.WILD_D4));
        }
    }
// method to shuffle the deck
    public void shuffle() {
        for (int i=0; i<SHUFFLE * cardsList.size(); i++) {
            int x = rand.nextInt(cardsList.size());
            int y = rand.nextInt(cardsList.size());
            Card temp = cardsList.get(x);
            cardsList.set(x,cardsList.get(y));
            cardsList.set(y,temp);
        }
    }

    //method to check if the deck is empty
    public boolean isEmpty() {
        return cardsList.size() == 0;
    }
// method to draw a card
    public Card draw() throws Empty_Exception {
        if (cardsList.size() == 0) {
            throw new Empty_Exception();
        }
        return cardsList.remove(0);
    }

    //method to discard a card
    public void discard(Card c) { 
        discardedCards.add(c);
    }
// method to mix the discarded pile to the deck and shuffle
    public void Shuffle_Mix() {
        cardsList.addAll(discardedCards);
        discardedCards.clear();
        shuffle();
    }

    ArrayList<Card> getDiscardedCards() {
        return discardedCards;
    }

   
}
