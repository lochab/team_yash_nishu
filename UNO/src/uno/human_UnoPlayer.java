package uno;

import java.util.List;

import java.util.Scanner;
import uno.Uno_Player.Color;
import uno.Uno_Player.Type;
//represents a human player
public class human_UnoPlayer implements Uno_Player {

    public int play(List<Card> hand, Card upCard, Color calledColor,
        Game_State state) {
    	
    	Scanner keyboard = new Scanner(System.in);
		int j = -1;
		
    	do {
        	System.out.print("\nEnter the card you'd like to play (or \"draw\" to draw a card) : ");
        	
        	String cardName = keyboard.nextLine();
        	if (cardName.equalsIgnoreCase("draw"))
        		return -1;
        	
        	Card card = cardName2Card(cardName);

    		for (int i=0; i<hand.size(); i++) {
    			if (hand.get(i).isTheSameCard(card))
    				j = i;
    		}
    		if (j==-1) {
    			System.out.println("This card does not appear in your hand!");
    		}
    		else if (canPlayOn(hand.get(j), upCard, calledColor)) {
        		return j;
        	}
        	else {
        		System.out.println("This card is not valid!");
        		j = -1;
        	}
    	} while (j == -1);
    				
        return -1;
    }

    public Color addColor(List<Card> hand) {
    	Scanner keyboard = new Scanner(System.in);

    	do {
    		System.out.print("Choose the color (R Y G B) : ");
    		String color = keyboard.nextLine();
    		
    		switch (color.charAt(0)) {
    		case 'R':
    			return Uno_Player.Color.RED;
    		case 'Y':
    			return Uno_Player.Color.YELLOW;
    		case 'G':
    			return Uno_Player.Color.GREEN;
    		case 'B':
    			return Uno_Player.Color.BLUE;
    		default:
    			System.out.println("Invalid color!");
    		}
    	} while (true);
    	
    }
    
    public Card cardName2Card(String cardName) {
    	Uno_Player.Color color = Uno_Player.Color.NONE;
    	Uno_Player.Type type = Uno_Player.Type.VALUE;
    	int	number = -1;
    	
    	char ch = cardName.charAt(0);
    	switch (ch) {
    		case 'R' :
    			color = Uno_Player.Color.RED;
    			break;
    		case 'Y' :
    			color = Uno_Player.Color.YELLOW;
    			break;
    		case 'G' :
    			color = Uno_Player.Color.GREEN;
    			break;
    		case 'B' :
    			color = Uno_Player.Color.BLUE;
    			break;
    		case 'W' :
    			color = Uno_Player.Color.NONE;
    			number = -1;
    			if (cardName.length() == 2)
    				type = Uno_Player.Type.WILD_D4;
    			else
    				type = Uno_Player.Type.WILD;
    			break;
    	}
    	if (color != Uno_Player.Color.NONE) {
    		ch = cardName.charAt(1);
    		if (Character.isDigit(ch)) {
    			type = Uno_Player.Type.VALUE;
    			number = ch - '0';
    		}
    		else if (ch == 'R') {
    			type = Uno_Player.Type.REVERSE;
    			number = -1;
    		}
    		else if (ch == 'S') {
    			type = Uno_Player.Type.SKIP;
    			number = -1;
    		}
    		else if (ch == '+' && cardName.charAt(2) == '2') {
    			type = Uno_Player.Type.DRAW_TWO;
    			number = -1;
    		}
    	}
    	
    	return ((number == -1) ? new Card(color, type) : new Card(color, number));
    }
    
    public boolean canPlayOn(Card card, Card upCard, Color calledColor) {

        if (card.getType() == Type.WILD ||
            card.getType() == Type.WILD_D4 ||
            card.getColor() == upCard.getColor() ||
            card.getColor() == calledColor ||
            (card.getType() == upCard.getType() &&
                card.getType() != Type.VALUE) ||
            card.getValue() == upCard.getValue() &&
                card.getType() == Type.VALUE &&
                upCard.getType() == Type.VALUE) {
            return true;
        }
        return false;
    }

}
