
package uno;

import java.util.List;

public class Game_State {

    private Game theGame;
    private int[] numCardsInHandsOfNextPlayers;
    private Uno_Player.Color[] mostRecentColor;
    private int[] totalScoreofPlayers;

    //constructor
    Game_State() {
        numCardsInHandsOfNextPlayers = new int[4];
        mostRecentColor = new Uno_Player.Color[4];
        totalScoreofPlayers = new int[4];
    }

    //constructor
    Game_State(Game game) {

        numCardsInHandsOfNextPlayers =
            new int[game.scoreBoard.getNumPlayers()];
        mostRecentColor =
            new Uno_Player.Color[game.scoreBoard.getNumPlayers()];
        totalScoreofPlayers =
            new int[game.scoreBoard.getNumPlayers()];

        if (game.direction == Game.Direction.FORWARD) {
            for (int i=0; i<game.h.length; i++) {
                numCardsInHandsOfNextPlayers[i] =
                    game.h[(game.currentPlayer + i + 1) %
                        game.scoreBoard.getNumPlayers()].size();
                totalScoreofPlayers[i] =
                    game.scoreBoard.getScore((game.currentPlayer + i + 1) %
                        game.scoreBoard.getNumPlayers());
                mostRecentColor[i] =
                    game.mostRecentColorCalled[(game.currentPlayer + i + 1) %
                        game.scoreBoard.getNumPlayers()];
            }
        }
        else {
            for (int i=0; i<game.h.length; i++) { // FIXTHIS
                numCardsInHandsOfNextPlayers[i] =
                    game.h[(game.currentPlayer - i - 1 +
                        game.scoreBoard.getNumPlayers()) %
                        game.scoreBoard.getNumPlayers()].size();
                totalScoreofPlayers[i] =
                    game.scoreBoard.getScore((game.currentPlayer - i - 1 +
                        game.scoreBoard.getNumPlayers()) %
                        game.scoreBoard.getNumPlayers());
                mostRecentColor[i] =
                    game.mostRecentColorCalled[(game.currentPlayer - i - 1 +
                        game.scoreBoard.getNumPlayers()) %
                        game.scoreBoard.getNumPlayers()];
            }
        }
        theGame = game;
    }

    
    public int[] getNumCardsInHandsOfNextPlayers() {
        return numCardsInHandsOfNextPlayers;
    }

    public int[] getTotalScoreOfNextPlayers() {
        return numCardsInHandsOfNextPlayers;
    }

    
    public Uno_Player.Color[] getMostRecentColorCalledByUpcomingPlayers() {
        return mostRecentColor;
    }

    //get played cards
    public List<Card> getPlayedCards() {
        if (theGame != null) {
            return theGame.deck.getDiscardedCards();
        }
        else {
            // testing only
            return new java.util.ArrayList<Card>();
        }
    }
}
