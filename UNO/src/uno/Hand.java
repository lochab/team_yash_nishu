
package uno;

import java.util.ArrayList;


public class Hand {

    private ArrayList<Card> cards;
    private Uno_Player player;
    private String playerName;
    private boolean isHuman = false;

   //constructor
    public Hand(String unoPlayerClassName, String playerName) {
        try {
            player = (Uno_Player)
                Class.forName(unoPlayerClassName).newInstance();
        }
        catch (Exception e) {
            System.out.println("Problem with " + unoPlayerClassName + ".");
            e.printStackTrace();
            System.exit(1);
        }
        if (unoPlayerClassName.equalsIgnoreCase("uno.human_UnoPlayer"))
        	isHuman = true;
        this.playerName = playerName;
        cards = new ArrayList<Card>();
    }

    //add a card to the hand
    void addCard(Card c) {
        cards.add(c);
    }

    // get the size of arraylist
    public int size() {
        return cards.size();
    }
    
   //check if player is human
    public boolean isHuman() {
    	return isHuman;
    }
 
// play game
    Card play(Game game) {
        int playedCard;
        playedCard = player.play(cards, game.getTopCard(), game.color,
            game.getGameState());
        if (playedCard == -1) {
            return null;
        }
        else {
            Card toPlay = cards.remove(playedCard);
            return toPlay;
        }
    }

    
    Uno_Player.Color addColor(Game game) {
        return player.addColor(cards);
    }

   
    public boolean isEmpty() {
        return cards.size() == 0;
    }

    
    public String toString() {
        String val = "";
        for (int i=0; i<cards.size(); i++) {
            val += cards.get(i);
            if (i<cards.size()-1) {
                val += ",";
            }
        }
        return val;
    }

    
    public int countCards() {
        int total = 0;
        for (int i=0; i<cards.size(); i++) {
            total += cards.get(i).forfeit();
        }
        return total;
    }

    public String getPlayerName() {
        return playerName;
    }
}
